import pandas as pd
from django.views import View

class GetWordFile(View):

    def build_word(self):
        writer = pd.ExcelWriter('demo.xlsx', engine='xlsxwriter')
        writer.close()
        return writer

    def get(self, request):
        document = self.build_word()
        return document
