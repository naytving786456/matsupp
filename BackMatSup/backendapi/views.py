import datetime
import jwt
import pandas as pd
from openpyxl import load_workbook
from docx import Document
from jwt import ExpiredSignatureError, InvalidSignatureError, DecodeError
from django.conf import settings
from django.http import Http404, HttpResponse, JsonResponse
from django.shortcuts import get_object_or_404
from rest_framework.generics import ListAPIView
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.pagination import PageNumberPagination
from rest_framework import status
from .models import *
from .serializers import *
from .auth import CustomAuthMiddleware, authenticate

student_grade = ['СПО', "CПO", "СПO", "CПО", "спо", "cпо", "cпo", "Среднее профессиональное образование",
                 "среднее профессиональное образование", "Среднее проффесиональное образование"]


class StudentSignInView(APIView):
    '''
    Вход студента
    todo Дополнить проверкой через LDAP, если пользователь есть в системе старой, скопировать его данные в БД и далее проверяем по логину и паролю
    '''

    def post(self, request):
        login = request.data.get('login')
        password = request.data.get('password')

        try:
            s = Student.objects.get(login=login, password=password)
        except Student.DoesNotExist:
            return Response({'detail': 'Неверный логин или пароль!'}, 404)

        return Response({
            **authenticate(s),
            'id': s.id,
            'fio': s.fio(),
            'financingSource': s.source_finance,
            'avatarUrl': s.avatar,
        })


class AdminSignInView(APIView):
    '''
    Вход админа
    '''

    def post(self, request):
        print("hello")
        login = request.data.get('login')
        password = request.data.get('password')

        try:
            s = Admin.objects.get(login=login, password=password)
        except Admin.DoesNotExist:
            return Response({'detail': 'Неверный логин или пароль!'}, 404)

        return Response({**authenticate(s), 'id': s.id, 'financingSource': s.lastname, 'avatarUrl': s.avatar})


class UpdateAccessTokenView(APIView):

    def post(self, request):
        refresh = request.data.get('refresh_token')

        try:
            data = jwt.decode(refresh, settings.SECRET_KEY, [settings.ALGORITHM],
                              {'verify_exp': True, 'verify_signature': True})

            if data['role'] == 'student':
                u = get_object_or_404(Student, id=data['id'])
            else:
                u = get_object_or_404(Admin, id=data['id'])

            return Response({**authenticate(u), 'refresh_token': refresh})

        except ExpiredSignatureError:
            return Response({'detail': 'Refresh token expired, login required!'}, 401)
        except InvalidSignatureError:
            return Response({'detail': 'Invalid signature of token!'}, 401)
        except DecodeError:
            return Response({'detail': 'Can not decode token!'}, 401)


class GetStudentDataView(CustomAuthMiddleware, APIView):

    def post(self, request):
        s = get_object_or_404(Student, id=request.data['id'])
        token = request.headers['Authorization'].split('Bearer ')[1]

        data = jwt.decode(token, settings.SECRET_KEY, [settings.ALGORITHM],
                          {'verify_exp': True, 'verify_signature': True})

        if data['role'] != 'student':
            return Response({'detail': 'No credentials!'}, 403)

        return Response({
            'id': s.id,
            'fio': s.fio(),
            'avatarUrl': s.avatar,
            'learningPlans': [s.learningPlan]
        })


class GetAdminDataView(CustomAuthMiddleware, APIView):

    def post(self, request):
        a = get_object_or_404(Admin, id=request.data['id'])
        token = request.headers['Authorization'].split('Bearer ')[1]

        data = jwt.decode(token, settings.SECRET_KEY, [settings.ALGORITHM],
                          {'verify_exp': True, 'verify_signature': True})

        if data['role'] != 'admin':
            return Response({'detail': 'No credentials!'}, 403)

        return Response({
            'id': a.id,
            'fio': a.fio(),
            'avatarUrl': a.avatar,
        })


class SearchListView(CustomAuthMiddleware, APIView):

    def get_fio(self,mas):

        if len(mas) == 1:
            try:
                lastname = mas[0]
            except:
                lastname = ''

            firstname = ''
            patronymic = ''
            return lastname, firstname, patronymic

        elif len(mas) == 2:
            try:
                lastname = mas[0]
                firstname = mas[1]

            except:
                lastname = ''
                firstname = ''

            patronymic = ''
            return lastname, firstname, patronymic
        elif len(mas) == 3:
            try:
                lastname = mas[0]
                firstname = mas[1]
                patronymic = mas[2]

            except:
                lastname=''
                firstname=''
                patronymic=''
            return lastname, firstname, patronymic



    def post(self, request):
        fio = self.get_fio(request.data['fio'].strip().split(" "))
        search_result = []
        students = []
        print(fio)

        # ПОИСК ПО КРИТЕРИЯЕМ, КАМПАНИЯМ,СТАТУСУ

        if (request.data["company_pk"] == "-1" and request.data["criterion"] == "-1" and request.data[
            "status"] == "-1" and fio[0] == "" and fio[1] == "" and fio[2] == ""):
            print("1")
            search_result = Request.objects.exclude(isDeleted=True)
        elif (request.data["company_pk"] == "-1" and request.data["criterion"] == "-1" and request.data[
            "status"] != "-1" and fio[0] == "" and fio[1] == "" and fio[2] == ""):
            print("2")
            search_result = Request.objects.filter(last_status=request.data["status"]).exclude(isDeleted=True)
        elif (request.data["company_pk"] == "-1" and request.data["criterion"] != "-1" and request.data[
            "status"] == "-1" and fio[0] == "" and fio[1] == "" and fio[2] == ""):
            print("3")
            criterion_search = DictCriterion.objects.filter(name=request.data["criterion"])[0]
            search_result = Request.objects.filter(criterion=criterion_search).exclude(isDeleted=True)
        elif (request.data["company_pk"] == "-1" and request.data["criterion"] != "-1" and request.data[
            "status"] != "-1" and fio[0] == "" and fio[1] == "" and fio[2] == ""):
            print("4")
            criterion_search = DictCriterion.objects.filter(name=request.data["criterion"])[0]
            search_result = Request.objects.filter(criterion=criterion_search,
                                                   last_status=request.data["status"]).exclude(isDeleted=True)
        elif (request.data["company_pk"] != "-1" and request.data["criterion"] == "-1" and request.data[
            "status"] == "-1" and fio[0] == "" and fio[1] == "" and fio[2] == ""):
            print("5")
            compaing = Compaing.objects.filter(id=request.data["company_pk"])[0]
            search_result = Request.objects.filter(compaing=compaing).exclude(isDeleted=True)
        elif (request.data["company_pk"] != "-1" and request.data["criterion"] == "-1" and request.data[
            "status"] != "-1" and fio[0] == "" and fio[1] == "" and fio[2] == ""):
            print("6")
            compaing = Compaing.objects.filter(id=request.data["company_pk"])[0]
            search_result = Request.objects.filter(compaing=compaing, last_status=request.data["status"]).exclude(
                isDeleted=True)
        elif (request.data["company_pk"] != "-1" and request.data["criterion"] != "-1" and request.data[
            "status"] == "-1" and fio[0] == "" and fio[1] == "" and fio[2] == ""):
            print("7")
            criterion_search = DictCriterion.objects.filter(name=request.data["criterion"])[0]
            compaing = Compaing.objects.filter(id=request.data["company_pk"])[0]
            search_result = Request.objects.filter(compaing=compaing, criterion=criterion_search).exclude(
                isDeleted=True)
        elif (request.data["company_pk"] != "-1" and request.data["criterion"] != "-1" and request.data[
            "status"] != "-1" and fio[0] == "" and fio[1] == "" and fio[2] == ""):
            print("8")
            criterion_search = DictCriterion.objects.get(name=request.data["criterion"])
            compaing = Compaing.objects.filter(id=request.data["company_pk"])[0]
            search_result = Request.objects.filter(compaing=compaing, criterion=criterion_search,
                                                   last_status=request.data["status"]).exclude(isDeleted=True)

        # ПОИСК ПО ФИО

        elif (request.data["company_pk"] == "-1" and request.data["criterion"] == "-1" and request.data[
            "status"] == "-1" and fio[0] != '' and fio[1] == '' and fio[2] == ''):
            print("9")
            try:
                students += Student.objects.filter(lastname__icontains=fio[0])
            except Exception as err:
                print(err)
            try:
                students += Student.objects.filter(firstname__icontains=fio[0])
            except Exception as err:
                print(err)

            for element in students:
                if len(Request.objects.exclude(isDeleted=True).filter(student=element)) >= 1:
                    search_result += Request.objects.exclude(isDeleted=True).filter(student=element)

# """##############"""

        elif (request.data["company_pk"] == "-1" and request.data["criterion"] == "-1" and request.data[
            "status"] == "-1" and fio[0] != '' and fio[1] != '' and fio[2] == ''):
            print("10")
            try:
                students += Student.objects.filter(lastname__icontains=fio[0]).filter(firstname__icontains=fio[1])
            except Exception as err:
                print(err)
            try:
                students += Student.objects.filter(lastname__icontains=fio[1]).filter(firstname__icontains=fio[0])
            except Exception as err:
                print(err)
            for element in students:
                if len(Request.objects.exclude(isDeleted=True).filter(student=element)) >= 1:
                    search_result += Request.objects.exclude(isDeleted=True).filter(student=element)

# """##############"""

        elif (request.data["company_pk"] == "-1" and request.data["criterion"] == "-1" and request.data[
            "status"] == "-1" and fio[0] != '' and fio[1] != '' and fio[2] != ''):
            print("11")
            try:
                students += Student.objects.filter(lastname__icontains=fio[0]).filter(
                    firstname__icontains=fio[1]).filter(patronymic__icontains=fio[2])
            except Exception as err:
                print(err)
            try:
                students += Student.objects.filter(lastname__icontains=fio[1]).filter(
                    firstname__icontains=fio[0]).filter(patronymic__icontains=fio[2])
            except Exception as err:
                print(err)
            for element in students:
                if len(Request.objects.exclude(isDeleted=True).filter(student=element)) >= 1:
                    search_result += Request.objects.exclude(isDeleted=True).filter(student=element)

# """#############"""

        # ПОИСК ПО ФАМИЛИИ И КРИТЕРИЯМ,СТАТУСУ И КАМПАНИЯМ

        elif (request.data["company_pk"] == "-1" and request.data["criterion"] == "-1" and request.data[
            "status"] != "-1" and fio[0] != '' and fio[1] == '' and fio[2] == ''):
            print("12")
            try:
                students += Student.objects.filter(lastname__icontains=fio[0])
            except Exception as err:
                print(err)
            try:
                students += Student.objects.filter(firstname__icontains=fio[0])
            except Exception as err:
                print(err)

            for element in students:
                if len(Request.objects.filter(last_status=request.data["status"]).filter(student=element).exclude(
                        isDeleted=True)) >= 1:
                    search_result += Request.objects.filter(last_status=request.data["status"]).filter(
                        student=element).exclude(isDeleted=True)
# """#############"""

        elif (request.data["company_pk"] == "-1" and request.data["criterion"] != "-1" and request.data[
            "status"] == "-1" and fio[0] != '' and fio[1] == '' and fio[2] == ''):
            print("13")
            try:
                students += Student.objects.filter(lastname__icontains=fio[0])
            except Exception as err:
                print(err)
            try:
                students += Student.objects.filter(firstname__icontains=fio[0])
            except Exception as err:
                print(err)
            criterion_search = DictCriterion.objects.get(name=request.data["criterion"])
            for element in students:
                if len(Request.objects.filter(criterion=criterion_search).filter(student=element).exclude(
                        isDeleted=True)) >= 1:
                    search_result += Request.objects.filter(criterion=criterion_search).filter(student=element).exclude(
                        isDeleted=True)
# """#############"""
        elif (request.data["company_pk"] == "-1" and request.data["criterion"] != "-1" and request.data[
            "status"] != "-1" and fio[0] != '' and fio[1] == '' and fio[2] == ''):
            print("14")
            try:
                students += Student.objects.filter(lastname__icontains=fio[0])
            except Exception as err:
                print(err)
            try:
                students += Student.objects.filter(firstname__icontains=fio[0])
            except Exception as err:
                print(err)

            criterion_search = DictCriterion.objects.get(name=request.data["criterion"])
            for element in students:
                if len(Request.objects.filter(criterion=criterion_search, last_status=request.data["status"]).filter(
                        student=element).exclude(isDeleted=True)) >= 1:
                    search_result += Request.objects.filter(criterion=criterion_search,
                                                            last_status=request.data["status"]).filter(
                        student=element).exclude(isDeleted=True)
# """#############"""

        elif (request.data["company_pk"] != "-1" and request.data["criterion"] == "-1" and request.data[
            "status"] == "-1" and fio[0] != '' and fio[1] == '' and fio[2] == ''):
            print("15")
            try:
                students += Student.objects.filter(lastname__icontains=fio[0])
            except Exception as err:
                print(err)
            try:
                students += Student.objects.filter(firstname__icontains=fio[0])
            except Exception as err:
                print(err)
            compaing = Compaing.objects.get(id=request.data["company_pk"])
            for element in students:
                if len(Request.objects.filter(compaing=compaing).filter(student=element).exclude(isDeleted=True)) >= 1:
                    search_result += Request.objects.filter(compaing=compaing).filter(student=element).exclude(
                        isDeleted=True)
                    print(search_result)
# """#############"""
        elif (request.data["company_pk"] != "-1" and request.data["criterion"] == "-1" and request.data[
            "status"] != "-1" and fio[0] != '' and fio[1] == '' and fio[2] == ''):
            print("16")
            try:
                students += Student.objects.filter(lastname__icontains=fio[0])
            except Exception as err:
                print(err)
            try:
                students += Student.objects.filter(firstname__icontains=fio[0])
            except Exception as err:
                print(err)

            compaing = Compaing.objects.get(id=request.data["company_pk"])

            for element in students:
                if len(Request.objects.filter(compaing=compaing).filter(
                        last_status=request.data["status"]).filter(student=element).exclude(
                    isDeleted=True)) >= 1:
                    search_result += Request.objects.filter(compaing=compaing).filter(
                        last_status=request.data["status"]).filter(student=element).exclude(isDeleted=True)
# """#############"""
        elif (request.data["company_pk"] != "-1" and request.data["criterion"] != "-1" and request.data[
            "status"] == "-1" and fio[0] != '' and fio[1] == '' and fio[2] == ''):
            print("17")
            try:
                students += Student.objects.filter(lastname__icontains=fio[0])
            except Exception as err:
                print(err)
            try:
                students += Student.objects.filter(firstname__icontains=fio[0])
            except Exception as err:
                print(err)

            criterion_search = DictCriterion.objects.get(name=request.data["criterion"])
            compaing = Compaing.objects.get(id=request.data["company_pk"])

            for element in students:
                if len(Request.objects.filter(compaing=compaing, criterion=criterion_search).filter(
                        student=element).exclude(
                    isDeleted=True)) >= 1:
                    search_result += Request.objects.filter(compaing=compaing, criterion=criterion_search).filter(
                        student=element).exclude(isDeleted=True)
# """#############"""
        elif (request.data["company_pk"] != "-1" and request.data["criterion"] != "-1" and request.data[
            "status"] != "-1" and fio[0] != '' and fio[1] == '' and fio[2] == ''):
            print("18")
            try:
                students += Student.objects.filter(lastname__icontains=fio[0])
            except Exception as err:
                print(err)
            try:
                students += Student.objects.filter(firstname__icontains=fio[0])
            except Exception as err:
                print(err)
            criterion_search = DictCriterion.objects.get(name=request.data["criterion"])
            compaing = Compaing.objects.get(id=request.data["company_pk"])
            for element in students:
                if len(Request.objects.filter(compaing=compaing).filter(criterion=criterion_search).filter(
                        last_status=request.data["status"]).filter(student=element).exclude(isDeleted=True)) >= 1:
                    search_result += Request.objects.filter(compaing=compaing).filter(
                        criterion=criterion_search).filter(
                        last_status=request.data["status"]).filter(student=element).exclude(isDeleted=True)
# """#############"""

        # ПОИСК ПО ФАМИЛИИ, ИМЕНИ И КАМПАНИИ, КРИТЕРИЯМ, СТАТУСУ

        elif (request.data["company_pk"] == "-1" and request.data["criterion"] == "-1" and request.data[
            "status"] != "-1" and fio[0] != '' and fio[1] != '' and fio[2] == ''):
            print("19")
            try:
                students += Student.objects.filter(lastname__icontains=fio[0], firstname__icontains=fio[1])
            except Exception as err:
                print(err)
            try:
                students = Student.objects.filter(lastname__icontains=fio[1], firstname__icontains=fio[0])
            except Exception as err:
                print(err)
            for element in students:
                if len(Request.objects.filter(last_status=request.data["status"]).filter(
                        student=element).exclude(isDeleted=True)) >= 1:
                    search_result += Request.objects.filter(last_status=request.data["status"]).filter(
                        student=element).exclude(isDeleted=True)
# """#############"""
        elif (request.data["company_pk"] == "-1" and request.data["criterion"] != "-1" and request.data[
            "status"] == "-1" and fio[0] != '' and fio[1] != '' and fio[2] == ''):
            print("20")
            try:
                students += Student.objects.filter(lastname__icontains=fio[0], firstname__icontains=fio[1])
            except Exception as err:
                print(err)
            try:
                students = Student.objects.filter(lastname__icontains=fio[1], firstname__icontains=fio[0])
            except Exception as err:
                print(err)
            criterion_search = DictCriterion.objects.get(name=request.data["criterion"])
            for element in students:
                if len(Request.objects.filter(criterion=criterion_search).filter(student=element).exclude(
                        isDeleted=True)) >= 1:
                    search_result += Request.objects.filter(criterion=criterion_search).filter(student=element).exclude(
                        isDeleted=True)
# """#############"""
        elif (request.data["company_pk"] == "-1" and request.data["criterion"] != "-1" and request.data[
            "status"] != "-1" and fio[0] != '' and fio[1] != '' and fio[2] == ''):
            print("21")
            try:
                students += Student.objects.filter(lastname__icontains=fio[0], firstname__icontains=fio[1])
            except Exception as err:
                print(err)
            try:
                students += Student.objects.filter(lastname__icontains=fio[1], firstname__icontains=fio[0])
            except Exception as err:
                print(err)

            criterion_search = DictCriterion.objects.get(name=request.data["criterion"])
            for element in students:
                if len(Request.objects.filter(criterion=criterion_search,
                                              last_status=request.data["status"]).filter(
                    student=element).exclude(isDeleted=True)) >= 1:
                    search_result += Request.objects.filter(criterion=criterion_search,
                                                            last_status=request.data["status"]).filter(
                        student=element).exclude(isDeleted=True)
# """#############"""
        elif (request.data["company_pk"] != "-1" and request.data["criterion"] == "-1" and request.data[
            "status"] == "-1" and fio[0] != '' and fio[1] != '' and fio[2] == ''):
            print("22")
            try:
                students += Student.objects.filter(lastname__icontains=fio[0], firstname__icontains=fio[1])
            except Exception as err:
                print(err)
            try:
                students += Student.objects.filter(lastname__icontains=fio[1], firstname__icontains=fio[0])
            except Exception as err:
                print(err)
            compaing = Compaing.objects.get(id=request.data["company_pk"])
            for element in students:
                if len(Request.objects.filter(compaing=compaing).filter(student=element).exclude(
                        isDeleted=True)) >= 1:
                    search_result += Request.objects.filter(compaing=compaing).filter(student=element).exclude(
                        isDeleted=True)
# """#############"""
        elif (request.data["company_pk"] != "-1" and request.data["criterion"] == "-1" and request.data[
            "status"] != "-1" and fio[0] != '' and fio[1] != '' and fio[2] == ''):
            print("23")
            try:
                students += Student.objects.filter(lastname__icontains=fio[0], firstname__icontains=fio[1])
            except Exception as err:
                print(err)
            try:
                students += Student.objects.filter(lastname__icontains=fio[1], firstname__icontains=fio[0])
            except Exception as err:
                print(err)
            compaing = Compaing.objects.get(id=request.data["company_pk"])
            for element in students:
                if len(Request.objects.filter(compaing=compaing, last_status=request.data["status"]).filter(
                        student=element).exclude(
                    isDeleted=True)) >= 1:
                    search_result += Request.objects.filter(compaing=compaing,
                                                            last_status=request.data["status"]).filter(
                        student=element).exclude(isDeleted=True)
# """#############"""
        elif (request.data["company_pk"] != "-1" and request.data["criterion"] != "-1" and request.data[
            "status"] == "-1" and fio[0] != '' and fio[1] != '' and fio[2] == ''):
            print("24")
            try:
                students += Student.objects.filter(lastname__icontains=fio[0], firstname__icontains=fio[1])
            except Exception as err:
                print(err)
            try:
                students += Student.objects.filter(lastname__icontains=fio[1], firstname__icontains=fio[0])
            except Exception as err:
                print(err)
            criterion_search = DictCriterion.objects.get(name=request.data["criterion"])
            compaing = Compaing.objects.get(id=request.data["company_pk"])
            for element in students:
                if len(Request.objects.filter(compaing=compaing, criterion=criterion_search).filter(
                        student=element).exclude(isDeleted=True)) >= 1:
                    search_result += Request.objects.filter(compaing=compaing, criterion=criterion_search).filter(
                        student=element).exclude(isDeleted=True)
# """#############"""
        elif (request.data["company_pk"] != "-1" and request.data["criterion"] != "-1" and request.data[
            "status"] != "-1" and fio[0] != '' and fio[1] != '' and fio[2] == ''):
            print("25")
            try:
                students += Student.objects.filter(lastname__icontains=fio[0], firstname__icontains=fio[1])
            except Exception as err:
                print(err)
            try:
                students += Student.objects.filter(lastname__icontains=fio[1], firstname__icontains=fio[0])
            except Exception as err:
                print(err)
            criterion_search = DictCriterion.objects.get(name=request.data["criterion"])
            compaing = Compaing.objects.get(id=request.data["company_pk"])
            for element in students:
                if len(Request.objects.filter(compaing=compaing, criterion=criterion_search,
                                              last_status=request.data["status"]).filter(
                    student=element).exclude(isDeleted=True)) >= 1:
                    search_result += Request.objects.filter(compaing=compaing, criterion=criterion_search,
                                                            last_status=request.data["status"]).filter(
                        student=element).exclude(isDeleted=True)
# """#############"""

        # ПОИСК ПО ФАМИЛИИ, ИМЕНИ, ОТЧЕСТВУ И КАМПАНИЯМ, КРИТЕРИЕРИЯМ, СТАТУСУ

        elif (request.data["company_pk"] == "-1" and request.data["criterion"] == "-1" and request.data[
            "status"] != "-1" and fio[0] != '' and fio[1] != '' and fio[2] != ''):
            print("26")
            try:
                students += Student.objects.filter(lastname__icontains=fio[0], firstname__icontains=fio[1],
                                                   patronymic__icontains=fio[2])
            except Exception as err:
                print(err)
            try:
                students += Student.objects.filter(lastname__icontains=fio[1], firstname__icontains=fio[0],
                                                   patronymic__icontains=fio[2])
            except Exception as err:
                print(err)

            for element in students:
                if len(Request.objects.filter(last_status=request.data["status"]).filter(
                        student=element).exclude(isDeleted=True)) >= 1:
                    search_result += Request.objects.filter(last_status=request.data["status"]).filter(
                        student=element).exclude(isDeleted=True)
# """#############"""
        elif (request.data["company_pk"] == "-1" and request.data["criterion"] != "-1" and request.data[
            "status"] == "-1" and fio[0] != '' and fio[1] != '' and fio[2] != ''):
            print("27")
            try:
                students += Student.objects.filter(lastname__icontains=fio[0], firstname__icontains=fio[1],
                                                   patronymic__icontains=fio[2])
            except Exception as err:
                print(err)
            try:
                students += Student.objects.filter(lastname__icontains=fio[1], firstname__icontains=fio[0],
                                                   patronymic__icontains=fio[2])
            except Exception as err:
                print(err)
            criterion_search = DictCriterion.objects.get(name=request.data["criterion"])
            for element in students:
                if len(Request.objects.filter(criterion=criterion_search).filter(student=element).exclude(
                        isDeleted=True)) >= 1:
                    search_result += Request.objects.filter(criterion=criterion_search).filter(student=element).exclude(
                        isDeleted=True)
# """#############"""
        elif (request.data["company_pk"] == "-1" and request.data["criterion"] != "-1" and request.data[
            "status"] != "-1" and fio[0] != '' and fio[1] != '' and fio[2] != ''):
            print("28")
            try:
                students += Student.objects.filter(lastname__icontains=fio[0], firstname__icontains=fio[1],
                                                   patronymic__icontains=fio[2])
            except Exception as err:
                print(err)
            try:
                students += Student.objects.filter(lastname__icontains=fio[1], firstname__icontains=fio[0],
                                                   patronymic__icontains=fio[2])
            except Exception as err:
                print(err)

            criterion_search = DictCriterion.objects.get(name=request.data["criterion"])
            for element in students:
                if len(Request.objects.filter(criterion=criterion_search,
                                              last_status=request.data["status"]).filter(
                    student=element).exclude(isDeleted=True)) >= 1:
                    search_result += Request.objects.filter(criterion=criterion_search,
                                                            last_status=request.data["status"]).filter(
                        student=element).exclude(isDeleted=True)
# """#############"""
        elif (request.data["company_pk"] != "-1" and request.data["criterion"] == "-1" and request.data[
            "status"] == "-1" and fio[0] != '' and fio[1] != '' and fio[2] != ''):
            print("29")
            try:
                students += Student.objects.filter(lastname__icontains=fio[0], firstname__icontains=fio[1],
                                                   patronymic__icontains=fio[2])
            except Exception as err:
                print(err)
            try:
                students += Student.objects.filter(lastname__icontains=fio[1], firstname__icontains=fio[0],
                                                   patronymic__icontains=fio[2])
            except Exception as err:
                print(err)
            compaing = Compaing.objects.get(id=request.data["company_pk"])
            for element in students:
                if len(Request.objects.filter(compaing=compaing).filter(student=element).exclude(
                        isDeleted=True)) >= 1:
                    search_result += Request.objects.filter(compaing=compaing).filter(student=element).exclude(
                        isDeleted=True)
# """#############"""
        elif (request.data["company_pk"] != "-1" and request.data["criterion"] == "-1" and request.data[
            "status"] != "-1" and fio[0] != '' and fio[1] != '' and fio[2] != ''):
            print("30")
            try:
                students += Student.objects.filter(lastname__icontains=fio[0], firstname__icontains=fio[1],
                                                   patronymic__icontains=fio[2])
            except Exception as err:
                print(err)
            try:
                students += Student.objects.filter(lastname__icontains=fio[1], firstname__icontains=fio[0],
                                                   patronymic__icontains=fio[2])
            except Exception as err:
                print(err)

            compaing = Compaing.objects.get(id=request.data["company_pk"])
            for element in students:
                if len(Request.objects.filter(compaing=compaing, last_status=request.data["status"]).filter(
                        student=element).exclude(
                    isDeleted=True)) >= 1:
                    search_result += Request.objects.filter(compaing=compaing,
                                                            last_status=request.data["status"]).filter(
                        student=element).exclude(
                        isDeleted=True)
# """#############"""
        elif (request.data["company_pk"] != "-1" and request.data["criterion"] != "-1" and request.data[
            "status"] == "-1" and fio[0] != '' and fio[1] != '' and fio[2] != ''):
            print("31")
            try:
                students += Student.objects.filter(lastname__icontains=fio[0], firstname__icontains=fio[1],
                                                   patronymic__icontains=fio[2])
            except Exception as err:
                print(err)
            try:
                students += Student.objects.filter(lastname__icontains=fio[1], firstname__icontains=fio[0],
                                                   patronymic__icontains=fio[2])
            except Exception as err:
                print(err)

            criterion_search = DictCriterion.objects.get(name=request.data["criterion"])
            compaing = Compaing.objects.get(id=request.data["company_pk"])
            for element in students:
                if len(Request.objects.filter(compaing=compaing, criterion=criterion_search).filter(
                        student=element).exclude(isDeleted=True)) >= 1:
                    search_result += Request.objects.filter(compaing=compaing, criterion=criterion_search).filter(
                        student=element).exclude(isDeleted=True)
# """#############"""
        elif (request.data["company_pk"] != "-1" and request.data["criterion"] != "-1" and request.data[
            "status"] != "-1" and fio[0] != '' and fio[1] != '' and fio[2] != ''):
            print("32")
            try:
                students += Student.objects.filter(lastname__icontains=fio[0], firstname__icontains=fio[1],
                                                   patronymic__icontains=fio[2])
            except Exception as err:
                print(err)
            try:
                students += Student.objects.filter(lastname__icontains=fio[1], firstname__icontains=fio[0],
                                                   patronymic__icontains=fio[2])
            except Exception as err:
                print(err)
            criterion_search = DictCriterion.objects.get(name=request.data["criterion"])
            compaing = Compaing.objects.get(id=request.data["company_pk"])
            for element in students:
                if len(Request.objects.filter(compaing=compaing, criterion=criterion_search,
                                              last_status=request.data["status"]).filter(
                    student=element).exclude(isDeleted=True)) >= 1:
                    search_result += Request.objects.filter(compaing=compaing, criterion=criterion_search,
                                                            last_status=request.data["status"]).filter(
                        student=element).exclude(isDeleted=True)

        serializer = RequestSerializer(search_result, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class RequestViewList(CustomAuthMiddleware, PageNumberPagination, APIView):
    page_size = 20

    def get(self, request):
        # фильтрация по студенту
        user = request.headers['Authorization'].split('Bearer ')[1]
        data = jwt.decode(user, settings.SECRET_KEY, [settings.ALGORITHM],
                          {'verify_exp': True, 'verify_signature': True})

        if data['role'] == 'student':
            requests = Request.objects.filter(student_id=data["id"]).exclude(isDeleted=True)
            serializer = RequestSerializer(requests, many=True)
            return Response(serializer.data)
        else:
            requests_admin = Request.objects.exclude(isDeleted=True).exclude(last_status="Черновик")
            page = self.paginate_queryset(requests_admin, request)
            serializer = RequestSerializer(page, many=True)
            return self.get_paginated_response(serializer.data)

    def post(self, request):
        req = Request.objects.create(
            sub_criterion=DictSubCriterion.objects.get(id=request.data['subCriterion']),
            student_id=request.data['student_id'],
            compaing_id=request.data['company_id'],
            last_status=request.data['last_status'],
            criterion=DictCriterion.objects.get(id=int(request.data['nomination'])),
        )

        return Response(RequestSerializer(req).data, status=status.HTTP_201_CREATED)


class RequestViewDetail(CustomAuthMiddleware, APIView):
    def get_object(self, pk):
        try:
            return Request.objects.get(pk=pk)

        except Request.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        _request = self.get_object(pk)
        serializer = RequestSerializer(_request)
        return Response(serializer.data)

    # def put(self, request, pk, format=None):
    #     _request = self.get_object(pk)
    #     serializer = RequestSerializer(_request, data=request.data)
    #     if serializer.is_valid():
    #         serializer.save()
    #         return Response(serializer.data)
    #     return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, pk):
        # этим запросом устанавливают статус заявки
        req = self.get_object(pk)
        req.last_status = request.data['status']
        req.save()

        return Response(status=204)

    def delete(self, request, pk):
        _request = self.get_object(pk)
        _request.isDeleted = True
        _request.save()
        return Response(status=status.HTTP_204_NO_CONTENT)


class RequestEditApplication(CustomAuthMiddleware, APIView):

    def get_object(self, pk):
        try:
            return Student.objects.get(pk=pk)
        except Student.DoesNotExist:
            raise Http404

    def post(self, request, pk, format=None):
        student = self.get_object(pk)
        serializer = StudentEditInfoSerializer(student, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class RequestFilesViewList(CustomAuthMiddleware, ListAPIView):
    serializer_class = RequestFileSerializer

    def get_queryset(self):
        return Request.objects.filter(id=self.request.query_params['pk'])[
            0].files.all()  # возвращает QuerySet с pk файлов , загруженных через данную страницу


class CompaingViewList(CustomAuthMiddleware, APIView):
    def get(self, request):
        compaing = Compaing.objects.exclude(isDeleted=True)
        serializer = CompaingSerializer(compaing, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = CompanyCreateSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status.HTTP_400_BAD_REQUEST)


class CompaingViewDetail(CustomAuthMiddleware, APIView):
    def get_object(self, pk):
        try:
            return Compaing.objects.get(pk=pk)
        except Request.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        compaing = self.get_object(pk)
        serializer = CompaingSerializer(compaing)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        compaing = self.get_object(pk)
        serializer = CompaingSerializer(compaing, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        compaing = self.get_object(pk)
        compaing.isDeleted = True
        compaing.save()
        return Response(status=status.HTTP_204_NO_CONTENT)


class CriterionViewList(CustomAuthMiddleware, APIView):
    def get(self, request):
        criterions = DictCriterion.objects.exclude(isDeleted=True)
        serializer = CriterionSerializer(criterions, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = CriterionSerializer(data=request.data)

        if serializer.is_valid():
            serializer.save()
            for index, value in enumerate(request.data['sub_criterion']):
                sub_criterion = DictSubCriterion.objects.create(
                    criterion=DictCriterion.objects.get(id=serializer.data['id']),
                    name=request.data['sub_criterion'][index]['name'],
                    paymentVPO=request.data['sub_criterion'][index]['paymentVo'],
                    paymentSPO=request.data['sub_criterion'][index]['paymentSpo'],
                )
                sub_criterion.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status.HTTP_400_BAD_REQUEST)


class CriterionViewDetail(CustomAuthMiddleware, APIView):
    def get_object(self, pk):
        try:
            return DictCriterion.objects.get(pk=pk)
        except Request.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        creterion = self.get_object(pk)
        serializer = RequestSerializer(creterion)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        criterion = self.get_object(pk)
        serializer = CriterionSerializer(criterion, data=request.data)
        if serializer.is_valid():
            serializer.save()
            for index, value in enumerate(request.data['sub_criterion']):
                try:
                    sub_criterion = DictSubCriterion.objects.get(id=value['id'])
                    sub_criterion.name = request.data['sub_criterion'][index]['name']
                    sub_criterion.paymentVPO = request.data['sub_criterion'][index]['paymentVPO']
                    sub_criterion.paymentSPO = request.data['sub_criterion'][index]['paymentSPO']
                    sub_criterion.save()
                except KeyError:
                    sub_criterion = DictSubCriterion.objects.create(
                        criterion=DictCriterion.objects.get(id=serializer.data['id']),
                        name=request.data['sub_criterion'][index]['name'],
                        paymentVPO=request.data['sub_criterion'][index]['paymentVPO'],
                        paymentSPO=request.data['sub_criterion'][index]['paymentSPO'],
                    )
                    sub_criterion.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        try:
            criterion = DictCriterion.objects.get(id=pk)

        except Request.DoesNotExist:
            raise Http404

        sub_criterion = DictSubCriterion.objects.filter(criterion=criterion.id)
        for index, value in enumerate(sub_criterion):
            value.isDeleted = True
            value.save()

        criterion.isDeleted = True
        criterion.save()
        return Response(status=status.HTTP_204_NO_CONTENT)


class SubCriterionView(CustomAuthMiddleware, APIView):

    def get(self, request, pk):
        sub_criterion = DictSubCriterion.objects.get(id=pk)
        serializer = SubCriterionSerializer(sub_criterion)
        return Response(serializer.data)


class CommentsViewList(CustomAuthMiddleware, APIView):
    def get(self, request):
        comments = Comments.objects.exclude(isDeleted=True)
        serializer = CommentsSerializer(comments, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = CommentsSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status.HTTP_400_BAD_REQUEST)


class NotificationListView(CustomAuthMiddleware, ListAPIView):
    serializer_class = ListNotificationSerializer
    queryset = Notification.objects.all()


class NotificationDetailView(CustomAuthMiddleware, APIView):

    def post(self, request):
        serializer = CreateNotificationSerializer(data=request.data)

        if serializer.is_valid():
            n = serializer.save()

            return Response({'id': n.id}, 201)

        return Response({"detail": serializer.errors}, 400)

    def delete(self, request):
        n = get_object_or_404(Notification, id=request.data['id'])

        n.delete()

        return Response(status=204)


class AddCommentView(CustomAuthMiddleware, APIView):

    def post(self, request):
        print(request.data)
        req = get_object_or_404(Request, id=request.data['id'])

        if request.data['text'].strip() == '':
            return Response({'detail': 'Комментрий не должен быть пустым!'}, 400)

        if request.data['role'] == 'student':
            req.comments.create(text=request.data['text'], student_id=request.data['user_id'])
        elif request.data['role'] == 'admin':
            req.comments.create(text=request.data['text'], admin_id=request.data['user_id'])

        return Response(status=201)


class SetImageView(CustomAuthMiddleware, APIView):

    def get_object(self, pk):
        try:
            return Request.objects.get(pk=pk)
        except Request.DoesNotExist:
            raise Http404

    def post(self, request):
        img = request.FILES['image']
        req = self.get_object(request.data['request'])
        # 10485760
        if img.size > 10485760:
            return Response({'detail': 'Файл должен быть до 10 МБ!'}, 400)

        path = './media/uploads/{}'.format(img.name.replace('.', f'.{now().timestamp()}.'))

        with open(path, 'wb') as writer:
            writer.write(img.read())

        file = RequestFiles.objects.create(linkDocs=path.split('.', maxsplit=1)[1])
        req.files.add(file)

        return Response(RequestFileSerializer(file).data, status=status.HTTP_201_CREATED)

    def delete(self, request):
        req = self.get_object(request.data['id'])
        req.files.remove(req.files.get(id=request.data['fileId']))

        return Response(status=status.HTTP_204_NO_CONTENT)


class GetExcelFile(CustomAuthMiddleware, APIView):

    def get_queryset(self, company, criterion):

        if company == criterion == "001":
            queryset = Request.objects.filter(isDeleted=False).exclude(last_status="Черновик")
            return queryset
        elif company == "001" and criterion != "001":
            queryset = Request.objects.filter(isDeleted=False).exclude(last_status="Черновик").filter(
                criterion=criterion)
            return queryset
        elif company != '001' and criterion == '001':
            queryset = Request.objects.filter(isDeleted=False).exclude(last_status="Черновик").filter(compaing=company)
            return queryset
        elif company != '001' and criterion != '001':
            queryset = Request.objects.filter(isDeleted=False).exclude(last_status="Черновик").filter(
                compaing=company).filter(criterion=criterion)
            return queryset

    def build_excel(self, company, criterion):
        file_name = f'Excel-Заявки.xlsx'
        path = f'media/files/{file_name}'
        json = []

        counter = 0

        queryset = self.get_queryset(company=company, criterion=criterion)
        for r in queryset:
            if r.sub_criterion == None or r.sub_criterion.name == 'Подкритериев нет':
                level = r.student.level == 'СПО'
                if level in student_grade:
                    counter += 1
                    data = {
                        "N#": counter,
                        "ФИО": r.student,
                        "Дата Рождения": r.student.birthday,
                        "Гражданство": r.student.citizenship,
                        "Серия паспорта": r.student.passport_seria,
                        "Номер паспорта": r.student.passport_number,
                        "ИНН": r.student.INN,
                        "СНИЛС": r.student.SNILS,
                        "Направление/Специальность": r.student.profile,
                        "Кластер": r.student.institut,
                        "Тип финансирования": r.student.source_finance,
                        "Курс": r.student.course,
                        "Уровень": r.student.level,
                        "Дата подачи заявления": r.CreatedOn.strftime("%d.%m.%Y %H:%M:%S"),
                        "Статус": r.last_status,
                        "Основание получения/Критерий": r.criterion.name,
                        "Основание получения/Подкритерий": '-',
                        "Сумма выплаты": r.criterion.paymentSPO
                    }
                    json.append(data)
                else:
                    counter += 1
                    data = {
                        "N#": counter,
                        "ФИО": r.student,
                        "Дата Рождения": r.student.birthday,
                        "Гражданство": r.student.citizenship,
                        "Серия паспорта": r.student.passport_seria,
                        "Номер паспорта": r.student.passport_number,
                        "ИНН": r.student.INN,
                        "СНИЛС": r.student.SNILS,
                        "Направление/Специальность": r.student.profile,
                        "Кластер": r.student.institut,
                        "Тип финансирования": r.student.source_finance,
                        "Курс": r.student.course,
                        "Уровень": r.student.level,
                        "Дата подачи заявления": r.CreatedOn.strftime("%d.%m.%Y %H:%M:%S"),
                        "Статус": r.last_status,
                        "Основание получения/Критерий": r.criterion.name,
                        "Основание получения/Подкритерий": '-',
                        "Сумма выплаты": r.criterion.paymentVPO
                    }
                    json.append(data)
            else:
                if level in student_grade:
                    counter += 1
                    data = {
                        "N#": counter,
                        "ФИО": r.student,
                        "Дата Рождения": r.student.birthday,
                        "Гражданство": r.student.citizenship,
                        "Серия паспорта": r.student.passport_seria,
                        "Номер паспорта": r.student.passport_number,
                        "ИНН": r.student.INN,
                        "СНИЛС": r.student.SNILS,
                        "Направление/Специальность": r.student.profile,
                        "Кластер": r.student.institut,
                        "Тип финансирования": r.student.source_finance,
                        "Курс": r.student.course,
                        "Уровень": r.student.level,
                        "Дата подачи заявления": r.CreatedOn.strftime("%d.%m.%Y %H:%M:%S"),
                        "Статус": r.last_status,
                        "Основание получения/Критерий": r.criterion.name,
                        "Основание получения/Подкритерий": r.sub_criterion.name,
                        "Сумма выплаты": r.sub_criterion.paymentSPO
                    }
                    json.append(data)
                else:
                    counter += 1
                    data = {
                        "N#": counter,
                        "ФИО": r.student,
                        "Дата Рождения": r.student.birthday,
                        "Гражданство": r.student.citizenship,
                        "Серия паспорта": r.student.passport_seria,
                        "Номер паспорта": r.student.passport_number,
                        "ИНН": r.student.INN,
                        "СНИЛС": r.student.SNILS,
                        "Направление/Специальность": r.student.profile,
                        "Кластер": r.student.institut,
                        "Тип финансирования": r.student.source_finance,
                        "Курс": r.student.course,
                        "Уровень": r.student.level,
                        "Дата подачи заявления": r.CreatedOn.strftime("%d.%m.%Y %H:%M:%S"),
                        "Статус": r.last_status,
                        "Основание получения/Критерий": r.criterion.name,
                        "Основание получения/Подкритерий": r.sub_criterion.name,
                        "Сумма выплаты": r.sub_criterion.paymentVPO
                    }

                    json.append(data)
        pd.DataFrame(json).to_csv(path, index=False, encoding='utf16', sep='\t')

        return path

    def post(self, request):
        path_to_file = self.build_excel(request.data.get('compaing_id'), request.data.get('typeMiracle_id'))
        content = {'url': path_to_file}
        return Response(content)


class GetWordFile(CustomAuthMiddleware, APIView):

    def build_word(self, company, criterion, level):

        part_one = "Оказать единовременную материальную поддержку в особых случаях следующим нуждающимся обучающимся " \
                   "очной формы обучения на бюджетной основе, "
        part_two = 'г., копия свидетельства ИНН, копия страхового пенсионного свидетельства, копия паспорта, ' \
                   'копия свидетельства о рождении, копия паспорта родителя, справка о составе семьи, документ, ' \
                   'подтверждающий выбранный критерий, документ, подтверждающий выбранный критерий, положительное ' \
                   'заключение председателя профсоюзной организации обучающихся от ВВЕДИТЕ ДАТУ ЗАСЕДАНИЯ г.);'
        name = 'Word-заявки.docx'
        path = f'media/files/{name}'
        document = Document()
        """Счетчик критериев для рендеринга на word файл"""
        crt_counter = 0

        """level == 0, Выплата для высшего образования"""

        if level == '0':
            """001 означает все кампании или все критерии, или и то и другое"""
            if criterion == company == "001":
                for crt in DictCriterion.objects.all():
                    if crt.sub_criterion.all():

                        crt_counter += 1
                        sub_counter = 0

                        document.add_paragraph(
                            f'{crt_counter}    {part_one} {crt.name}:')
                        for sub_crt in crt.sub_criterion.all():
                            sub_counter += 1
                            req_counter = 0
                            document.add_paragraph(
                                f'{sub_counter}.   в размере {sub_crt.paymentVPO} рублей:')

                            for req in Request.objects.filter(isDeleted=False).filter(last_status='Принято').filter(
                                    sub_criterion=sub_crt):
                                student = Student.objects.get(id=req.student.id)
                                if student.level not in student_grade:
                                    req_counter += 1
                                    document.add_paragraph(
                                        f"{sub_counter}.{req_counter}    {req.student} – {req.student.course} год обучения {req.student.level}, направление «{req.student.profile}» (заявление от {req.CreatedOn.strftime('%d.%m.%Y %H:%M:%S')} {part_two}")
                    else:
                        crt_counter += 1
                        req_counter = 0
                        document.add_paragraph(
                            f'{crt_counter}    {part_one}{crt.name} в размере {crt.paymentVPO} рублей.')

                        for req in Request.objects.filter(isDeleted=False).filter(last_status='Принято').filter(
                                criterion=crt):
                            student = Student.objects.get(id=req.student.id)
                            if student.level not in student_grade:
                                req_counter += 1
                                document.add_paragraph(
                                    f"{crt_counter}.{req_counter}    {req.student} – {req.student.course} год обучения {req.student.level}, направление «{req.student.profile}» (заявление от {req.CreatedOn.strftime('%d.%m.%Y %H:%M:%S')} {part_two}")


            elif company == "001" and criterion != '001':

                crt = DictCriterion.objects.get(id=criterion)
                sub_counter = 0
                if crt.sub_criterion.all():
                    document.add_paragraph(f'1     {part_one}{crt.name}:')
                    for sub_crt in crt.sub_criterion.all():
                        sub_counter += 1
                        document.add_paragraph(
                            f'{sub_counter}.   в размере {sub_crt.paymentVPO} рублей:')
                        req_counter = 0
                        for req in Request.objects.filter(isDeleted=False).filter(last_status='Принято').filter(
                                sub_criterion=sub_crt):
                            student = Student.objects.get(id=req.student.id)
                            if student.level not in student_grade:
                                req_counter += 1
                                document.add_paragraph(
                                    f"{sub_counter}.{req_counter}    {req.student} – {req.student.course} год обучения {req.student.level}, направление «{req.student.profile}» (заявление от {req.CreatedOn.strftime('%d.%m.%Y %H:%M:%S')} {part_two}")


                else:
                    document.add_paragraph(f'1     {part_one}{crt.name} в размере {crt.paymentVPO} рублей.')
                    req_counter = 0
                    for req in Request.objects.filter(isDeleted=False).filter(last_status='Принято').filter(
                            criterion=criterion):
                        student = Student.objects.get(id=req.student.id)
                        if student.level not in student_grade:
                            req_counter += 1
                            document.add_paragraph(
                                f"1.{req_counter}.{req.student} – {req.student.course} год обучения {req.student.level}, направление «{req.student.profile}» (заявление от {req.CreatedOn.strftime('%d.%m.%Y %H:%M:%S')} {part_two}")


            elif company != '001' and criterion == '001':
                crt_counter = 0
                for crt in DictCriterion.objects.all():
                    crt_counter += 1
                    sub_counter = 0

                    if crt.sub_criterion.all():

                        document.add_paragraph(
                            f'{crt_counter}    {part_one} {crt.name}:')
                        for sub_crt in crt.sub_criterion.all():
                            sub_counter += 1
                            document.add_paragraph(
                                f'{sub_counter}.   в размере {sub_crt.paymentVPO} рублей:')

                            req_counter = 0
                            for req in Request.objects.filter(isDeleted=False).filter(last_status='Принято').filter(
                                    compaing=company).filter(sub_criterion=sub_crt):
                                student = Student.objects.get(id=req.student.id)
                                if student.level not in student_grade:
                                    req_counter += 1

                                    document.add_paragraph(
                                        f"{sub_counter}.{req_counter}    {req.student} – {req.student.course} год обучения {req.student.level}, направление «{req.student.profile}» (заявление от {req.CreatedOn.strftime('%d.%m.%Y %H:%M:%S')} {part_two}")

                    else:

                        req_counter = 0
                        document.add_paragraph(
                            f'{crt_counter}    {part_one} {crt.name} в размере {crt.paymentVPO} рублей.')

                        for req in Request.objects.filter(isDeleted=False).filter(last_status='Принято').filter(
                                compaing=company).filter(criterion=crt):
                            student = Student.objects.get(id=req.student.id)
                            if student.level not in student_grade:
                                req_counter += 1
                                document.add_paragraph(
                                    f"{crt_counter}.{req_counter}    {req.student} – {req.student.course} год обучения {req.student.level}, направление «{req.student.profile}» (заявление от {req.CreatedOn.strftime('%d.%m.%Y %H:%M:%S')} {part_two}")


            elif company != '001' and criterion != '001':
                crt = DictCriterion.objects.get(id=criterion)
                if crt.sub_criterion.all():
                    document.add_paragraph(f'1    {part_one} {crt.name}:')
                    sub_counter = 0
                    for sub_crt in crt.sub_criterion.all():
                        sub_counter += 1
                        req_counter = 0
                        document.add_paragraph(
                            f'{sub_counter}.   в размере {sub_crt.paymentVPO} рублей:')

                        for req in Request.objects.filter(isDeleted=False).filter(last_status='Принято').filter(
                                sub_criterion=sub_crt).filter(compaing=company):

                            student = Student.objects.get(id=req.student.id)
                            if student.level not in student_grade:
                                req_counter += 1
                                document.add_paragraph(
                                    f"{sub_counter}.{req_counter}    {req.student} – {req.student.course} год обучения {req.student.level}, направление «{req.student.profile}» (заявление от {req.CreatedOn.strftime('%d.%m.%Y %H:%M:%S')} {part_two}")

                else:
                    req_counter = 0
                    document.add_paragraph(
                        f'1    {part_one} {crt.name} в размере {crt.paymentVPO} рублей.')

                    for req in Request.objects.filter(isDeleted=False).filter(last_status='Принято').filter(
                            criterion=crt).filter(compaing=company):
                        student = Student.objects.get(id=req.student.id)
                        if student.level not in student_grade:
                            req_counter += 1
                            document.add_paragraph(
                                f"1.{req_counter}    {req.student} – {req.student.course} год обучения {req.student.level}, направление «{req.student.profile}» (заявление от {req.CreatedOn.strftime('%d.%m.%Y %H:%M:%S')} {part_two}")

        elif level == '1':
            """001 означает все кампании или все критерии, или и то и другое"""
            if criterion == company == "001":
                for crt in DictCriterion.objects.all():
                    if crt.sub_criterion.all():

                        crt_counter += 1
                        sub_counter = 0

                        document.add_paragraph(
                            f'{crt_counter}    {part_one} {crt.name}:')
                        for sub_crt in crt.sub_criterion.all():
                            sub_counter += 1
                            req_counter = 0
                            document.add_paragraph(
                                f'{sub_counter}.   в размере {sub_crt.paymentSPO} рублей:')

                            for req in Request.objects.filter(isDeleted=False).filter(last_status='Принято').filter(
                                    sub_criterion=sub_crt):
                                student = Student.objects.get(id=req.student.id)
                                if student.level in student_grade:
                                    req_counter += 1
                                    document.add_paragraph(
                                        f"{sub_counter}.{req_counter}    {req.student} – {req.student.course} год обучения {req.student.level}, направление «{req.student.profile}» (заявление от {req.CreatedOn.strftime('%d.%m.%Y %H:%M:%S')} {part_two}")
                    else:
                        crt_counter += 1
                        req_counter = 0
                        document.add_paragraph(
                            f'{crt_counter}    {part_one}{crt.name} в размере {crt.paymentSPO} рублей.')

                        for req in Request.objects.filter(isDeleted=False).filter(last_status='Принято').filter(
                                criterion=crt):
                            student = Student.objects.get(id=req.student.id)
                            if student.level in student_grade:
                                req_counter += 1
                                document.add_paragraph(
                                    f"{crt_counter}.{req_counter}    {req.student} – {req.student.course} год обучения {req.student.level}, направление «{req.student.profile}» (заявление от {req.CreatedOn.strftime('%d.%m.%Y %H:%M:%S')} {part_two}")


            elif company == "001" and criterion != '001':

                crt = DictCriterion.objects.get(id=criterion)
                sub_counter = 0
                if crt.sub_criterion.all():
                    document.add_paragraph(f'1     {part_one}{crt.name}:')
                    for sub_crt in crt.sub_criterion.all():
                        sub_counter += 1
                        document.add_paragraph(
                            f'{sub_counter}.   в размере {sub_crt.paymentSPO} рублей:')
                        req_counter = 0
                        for req in Request.objects.filter(isDeleted=False).filter(last_status='Принято').filter(
                                sub_criterion=sub_crt):
                            student = Student.objects.get(id=req.student.id)
                            if student.level in student_grade:
                                req_counter += 1
                                document.add_paragraph(
                                    f"{sub_counter}.{req_counter}    {req.student} – {req.student.course} год обучения {req.student.level}, направление «{req.student.profile}» (заявление от {req.CreatedOn.strftime('%d.%m.%Y %H:%M:%S')} {part_two}")


                else:
                    document.add_paragraph(f'1     {part_one}{crt.name} в размере {crt.paymentSPO} рублей.')
                    req_counter = 0
                    for req in Request.objects.filter(isDeleted=False).filter(last_status='Принято').filter(
                            criterion=criterion):
                        student = Student.objects.get(id=req.student.id)
                        if student.level in student_grade:
                            req_counter += 1
                            document.add_paragraph(
                                f"1.{req_counter}.{req.student} – {req.student.course} год обучения {req.student.level}, направление «{req.student.profile}» (заявление от {req.CreatedOn.strftime('%d.%m.%Y %H:%M:%S')} {part_two}")


            elif company != '001' and criterion == '001':

                for crt in DictCriterion.objects.all():
                    crt_counter += 1
                    sub_counter = 0

                    if crt.sub_criterion.all():

                        document.add_paragraph(
                            f'{crt_counter}    {part_one} {crt.name}:')
                        for sub_crt in crt.sub_criterion.all():
                            sub_counter += 1
                            document.add_paragraph(
                                f'{sub_counter}.   в размере {sub_crt.paymentSPO} рублей:')

                            req_counter = 0
                            for req in Request.objects.filter(isDeleted=False).filter(last_status='Принято').filter(
                                    compaing=company).filter(sub_criterion=sub_crt):
                                student = Student.objects.get(id=req.student.id)
                                if student.level in student_grade:
                                    req_counter += 1

                                    document.add_paragraph(
                                        f"{sub_counter}.{req_counter}    {req.student} – {req.student.course} год обучения {req.student.level}, направление «{req.student.profile}» (заявление от {req.CreatedOn.strftime('%d.%m.%Y %H:%M:%S')} {part_two}")

                    else:

                        req_counter = 0
                        document.add_paragraph(
                            f'1    {part_one} {crt.name} в размере {crt.paymentSPO} рублей.')

                        for req in Request.objects.filter(isDeleted=False).filter(last_status='Принято').filter(
                                compaing=company).filter(criterion=crt):
                            student = Student.objects.get(id=req.student.id)
                            if student.level in student_grade:
                                req_counter += 1
                                document.add_paragraph(
                                    f"{crt_counter}.{req_counter}    {req.student} – {req.student.course} год обучения {req.student.level}, направление «{req.student.profile}» (заявление от {req.CreatedOn.strftime('%d.%m.%Y %H:%M:%S')} {part_two}")


            elif company != '001' and criterion != '001':
                crt = DictCriterion.objects.get(id=criterion)
                if crt.sub_criterion.all():
                    document.add_paragraph(f'1    {part_one} {crt.name}:')
                    sub_counter = 0
                    for sub_crt in crt.sub_criterion.all():
                        sub_counter += 1
                        req_counter = 0
                        document.add_paragraph(
                            f'{sub_counter}.   в размере {sub_crt.paymentSPO} рублей:')

                        for req in Request.objects.filter(isDeleted=False).filter(last_status='Принято').filter(
                                sub_criterion=sub_crt).filter(compaing=company):

                            student = Student.objects.get(id=req.student.id)
                            if student.level in student_grade:
                                req_counter += 1
                                document.add_paragraph(
                                    f"{sub_counter}.{req_counter}    {req.student} – {req.student.course} год обучения {req.student.level}, направление «{req.student.profile}» (заявление от {req.CreatedOn.strftime('%d.%m.%Y %H:%M:%S')} {part_two}")

                else:
                    req_counter = 0
                    document.add_paragraph(
                        f'1    {part_one} {crt.name} в размере {crt.paymentSPO} рублей.')

                    for req in Request.objects.filter(isDeleted=False).filter(last_status='Принято').filter(
                            criterion=crt).filter(compaing=company):
                        student = Student.objects.get(id=req.student.id)
                        if student.level in student_grade:
                            req_counter += 1
                            document.add_paragraph(
                                f"1.{req_counter}    {req.student} – {req.student.course} год обучения {req.student.level}, направление «{req.student.profile}» (заявление от {req.CreatedOn.strftime('%d.%m.%Y %H:%M:%S')} {part_two}")

        document.save(path)

        return path

    def post(self, request):
        get_document = self.build_word(request.data.get('compaing_id'), request.data.get('typeMiracle_id'),
                                       request.data.get('VoSpo'))
        return Response({"url": get_document})
