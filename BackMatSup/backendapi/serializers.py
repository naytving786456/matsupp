from rest_framework import serializers
from .models import *


class StudentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Student
        exclude = ['password', 'login']


class StudentEditInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Student
        exclude = ['password', 'login', "token", 'birthday', 'learningPlan', 'date_create_profile', 'avatar']


class AdminSerializer(serializers.ModelSerializer):
    class Meta:
        model = Admin
        exclude = ['password', 'login']


class CompaingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Compaing
        fields = '__all__'


class CompanyCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Compaing
        exclude = ['isDeleted', 'createdOn']


class SubCriterionSerializer(serializers.ModelSerializer):
    class Meta:
        model = DictSubCriterion
        exclude = ['createdOn', 'isDeleted']


class CriterionSerializer(serializers.ModelSerializer):
    sub_criterion = SubCriterionSerializer(many=True, read_only=True)

    class Meta:
        model = DictCriterion
        fields = ['id', 'name', 'docs', 'paymentVPO', 'paymentSPO', 'payment_status', 'sub_criterion']


class CommentsSerializer(serializers.ModelSerializer):
    student = StudentSerializer(read_only=True)
    admin = AdminSerializer(read_only=True)

    class Meta:
        model = Comments
        fields = '__all__'


class RequestSerializer(serializers.ModelSerializer):
    sub_criterion = SubCriterionSerializer(read_only=True)
    student = StudentSerializer(read_only=True)
    compaing = CompaingSerializer(read_only=True)
    criterion = CriterionSerializer(read_only=True)
    comments = CommentsSerializer(read_only=True, many=True)

    class Meta:
        model = Request
        fields = '__all__'


class RequestFileSerializer(serializers.ModelSerializer):
    class Meta:
        model = RequestFiles
        fields = '__all__'


class CreateNotificationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Notification
        fields = ['text']


class ListNotificationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Notification
        fields = ['id', 'text']


class ExportExcelWordSerializer(serializers.ModelSerializer):
    class Meta:
        model = ExcelWordFiles
        fields = '__all__'
