from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from django.conf.urls.static import static
from django.conf import settings

from . import views


urlpatterns = [
    path('companies/get/', views.CompaingViewList.as_view()),
    path('companies/create/', views.CompaingViewList.as_view()),
    path('companies/<int:pk>/', views.CompaingViewDetail.as_view()),

    path('criterion/get/', views.CriterionViewList.as_view()),
    path('criterion/<str:pk>/', views.CriterionViewDetail.as_view()),
    path('sub-criterion/get/', views.SubCriterionView.as_view()),

    path('comments/add/', views.CommentsViewList.as_view()),
    path('requests/get/', views.RequestViewList.as_view()),
    path('requests-files/get/', views.RequestFilesViewList.as_view()),
    path('add/document/<int:pk>', views.RequestFilesViewList.as_view()),
    path('requests/<str:pk>/', views.RequestViewDetail.as_view()),
    path('edit/application/<int:pk>/', views.RequestEditApplication.as_view()),

    path('search/request/', views.SearchListView.as_view()),

    path('notifications/get/', views.NotificationListView.as_view()),
    path('notifications/detail/', views.NotificationDetailView.as_view()),

    path('comments/create/', views.AddCommentView.as_view()),

    path('set-image/', views.SetImageView.as_view()),

    path('auth/student/login/', views.StudentSignInView.as_view()),
    path('auth/student/detail/', views.GetStudentDataView.as_view()),
    path('auth/admin/login/', views.AdminSignInView.as_view()),
    path('auth/admin/detail/', views.GetAdminDataView.as_view()),
    path('auth/refresh/', views.UpdateAccessTokenView.as_view()),

    path('get-word/', views.GetWordFile.as_view()),
    path('get-excel/', views.GetExcelFile.as_view()),


]

urlpatterns = format_suffix_patterns(urlpatterns)

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)