from django.contrib import admin
from .models import *


# Register your models here.

@admin.register(Student)
class PersonAdmin(admin.ModelAdmin):
    list_display = ("lastname", "firstname", "patronymic", "login", "password")
    ordering = ("lastname", "firstname")
    list_filter = ("source_finance", "course")
    search_fields = ("lastname", "firstname", "patronymic")

@admin.register(Request)
class RequsestAdmin(admin.ModelAdmin):
    list_filter = ['compaing__name','criterion__name']
    search_fields = ['student__lastname','student__firstname','student__patronymic']

admin.site.register(Admin)
admin.site.register(Compaing)
admin.site.register(DictCriterion)
admin.site.register(Comments)
admin.site.register(RequestFiles)
admin.site.register(Notification)
admin.site.register(DictSubCriterion)
