from django.core.exceptions import ValidationError
from django.core.management.base import BaseCommand
import json
from backendapi.models import Student
from datetime import datetime


class Command(BaseCommand):

    def get_right_date(self, date):
        try:
            if date.find(' ') != -1 and date != '01.01.0001 0:00:00':
                data = datetime.strftime(datetime.strptime(date[:date.index(' ')], "%d.%m.%Y"), "%Y-%m-%d")
            else:
                data = datetime.strftime(datetime.strptime(date, "%d.%m.%Y"), "%Y-%m-%d")
        except:
            data = datetime.strptime('2022-01-01', "%Y-%m-%d")
        return data

    def handle(self, *args, **options):

        with open('media/json/matsup.json', 'rb') as j:
            data = json.load(j)

            for index, value in enumerate(data):
                print(index)
                Student.objects.create(
                    login=value['login'],
                    password=value['password'],
                    token=value['GUID'],

                    lastname=value['Фамилия'],
                    firstname=value['Имя'],
                    patronymic=value['Отчество'],
                    birthday=self.get_right_date(value['ДатаРождения']),
                    learningPlan=value['Направление'],

                    phone=value['Телефон'],
                    institut=value['Институт'],
                    profile=value['Направление'],
                    form=value['ФормаОбучения'],
                    source_finance=value['Основа'],
                    level=value['УровеньПодготовки'],
                    course=value['Курс'],

                    address=value['АдресРегистрации'],
                    factadress=value['АдресПроживания'],

                    citizenship=value['Гражданство'],
                    INN=value['ИНН'],
                    SNILS=value['СНИЛС'],
                    passport_seria=value['Серия'],
                    passport_number=value['Номер'],
                    passport_IssueDate=self.get_right_date(value['ДатаВыдачи']),
                    passport_IssueBy=value['ОВД'],
                    passport_DepartmentCode=value['КодПодразделения'],

                    date_create_profile=self.get_right_date(value['ДатаНачалаОбучения']),
                )
