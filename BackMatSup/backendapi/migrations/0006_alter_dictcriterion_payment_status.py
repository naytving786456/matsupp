# Generated by Django 4.0.6 on 2023-02-06 22:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('backendapi', '0005_dictcriterion_payment_status_alter_student_token'),
    ]

    operations = [
        migrations.AlterField(
            model_name='dictcriterion',
            name='payment_status',
            field=models.CharField(default='False', max_length=10),
        ),
    ]
