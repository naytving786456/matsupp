# Generated by Django 4.0.6 on 2023-02-19 11:35

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('backendapi', '0017_request_sub_criterion'),
    ]

    operations = [
        migrations.AlterField(
            model_name='request',
            name='sub_criterion',
            field=models.ForeignKey(blank=True, default='Нет подкритерия', null=True, on_delete=django.db.models.deletion.SET_DEFAULT, to='backendapi.dictsubcriterion'),
        ),
    ]
