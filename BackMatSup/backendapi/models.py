from django.utils.timezone import now
from django.db import models


# Create your models here.


class Student(models.Model):
    login = models.CharField(blank=True, null=True, max_length=256)
    password = models.CharField(blank=True, null=True, max_length=256)
    token = models.UUIDField(max_length=256, blank=True, null=True)

    lastname = models.CharField("Фамилия",  blank=True, null=True, max_length=256)
    firstname = models.CharField("Имя", blank=True, null=True, max_length=256)
    patronymic = models.CharField("Отчество", blank=True, null=True, max_length=256)
    birthday = models.DateField("Дата рождения", blank=True, null=True,)
    learningPlan = models.CharField(max_length=255, blank=True, null=True,)

    phone = models.CharField("Телефон", max_length=256, blank=True, null=True)
    institut = models.CharField("Интститут", max_length=256, blank=True, null=True)
    profile = models.CharField("Направление", max_length=256, blank=True, null=True)
    form = models.CharField("Форма обучения", max_length=256, blank=True, null=True)
    source_finance = models.CharField("Источник финансирования", max_length=256, blank=True, null=True)
    level = models.CharField("Уровень обучения", max_length=256, blank=True, null=True)
    course = models.CharField("Курс", max_length=25, blank=True, null=True)

    address = models.CharField('Адрес по прописке', max_length=1024, blank=True, null=True)
    factadress = models.CharField('Фактический адрес проживания', max_length=1024, blank=True, null=True)

    citizenship = models.CharField('Гражданство', max_length=512, blank=True, null=True)
    INN = models.CharField('ИНН', max_length=12, blank=True, null=True)
    SNILS = models.CharField('СНИЛС', blank=True, null=True, max_length=20)
    passport_seria = models.CharField("Серия паспорта", max_length=20, blank=True, null=True)
    passport_number = models.CharField("Номер паспорта", max_length=100, blank=True, null=True)
    passport_IssueDate = models.DateField("Дата выдачи", blank=True, null=True)
    passport_IssueBy = models.TextField("Кем выдан", blank=True, null=True)
    passport_DepartmentCode = models.CharField("Код департамента", max_length=20, blank=True, null=True)

    isValidData = models.BooleanField("Данные верны", default=True)
    date_create_profile = models.DateField("Дата создания", blank=True, null=True)
    isDeleted = models.BooleanField(default=False)

    avatar = models.URLField(default="uploads/Default_picture.jpeg", blank=True, null=True)

    def __str__(self):
        return str(self.lastname + " " + self.firstname + " " + self.patronymic).strip()

    def fio(self):
        return f'{self.lastname} {self.firstname} {self.patronymic}'

    class Meta:
        verbose_name = 'Студент'
        verbose_name_plural = 'Студенты'


class Admin(models.Model):
    '''Админы'''
    login = models.CharField(unique=True, max_length=256)
    password = models.CharField(unique=True, max_length=256)

    lastname = models.CharField("Фамилия", max_length=256)
    firstname = models.CharField("Имя", max_length=256)
    patronymic = models.CharField("Отчество", max_length=256)
    is_staff = models.BooleanField(default=False)

    avatar = models.URLField()

    def __str__(self):
        return str(self.lastname + " " + self.firstname + " " + self.patronymic).strip()

    def fio(self):
        return f'{self.lastname} {self.firstname} {self.patronymic}'

    class Meta:
        verbose_name = 'Админ'
        verbose_name_plural = 'Админы'


class Compaing(models.Model):
    '''Кампания, создание помесячное(январь, февраль, и т д)'''
    name = models.CharField('Название кампании', max_length=256)
    startDate = models.DateTimeField(default=now)
    endDate = models.DateTimeField(default=now)
    isDeleted = models.BooleanField(default=False)
    createdOn = models.DateField("Дата создания", auto_now=True)

    def __str__(self):
        return self.name


class DictCriterion(models.Model):
    '''Справочкник критериев'''
    name = models.TextField('Название критерия')
    docs = models.TextField('Требуемые документы')
    paymentVPO = models.FloatField("Выплата для ВО", null=True, blank=True)
    paymentSPO = models.FloatField("Выплата для СПО", null=True, blank=True)
    payment_status = models.BooleanField(default=False)
    """payment_status добавляет возможность создания критериев для коммерции
        False -  только бюджет, True - для всех"""
    isDeleted = models.BooleanField(default=False)
    createdOn = models.DateField("Дата создания", default=now)

    def __str__(self):
        return self.name


class DictSubCriterion(models.Model):
    user_chose = models.ForeignKey(DictCriterion, blank=True, null=True, on_delete=models.CASCADE)
    criterion = models.ForeignKey(DictCriterion, related_name='sub_criterion', null=True, blank=True, default=None, on_delete=models.CASCADE)
    name = models.TextField(verbose_name='Название подкритерия', null=True, blank=True)
    paymentVPO = models.FloatField(verbose_name="Выплата для ВО", null=True, blank=True)
    paymentSPO = models.FloatField(verbose_name="Выплата для СПО", null=True, blank=True)
    isDeleted = models.BooleanField(default=False, null=True, blank=True)
    createdOn = models.DateField("Дата создания", null=True, blank=True, default=now)

    class Meta:
        unique_together = ['id', 'name', 'paymentVPO', 'paymentSPO', 'criterion']
        ordering = ['id']

    def __str__(self):
        return self.name


class Comments(models.Model):
    """ Комментарии """
    student = models.ForeignKey(Student, models.CASCADE, null=True, blank=True)
    admin = models.ForeignKey(Admin, models.CASCADE, null=True, blank=True)
    text = models.CharField(max_length=1000)
    created_at = models.DateTimeField(auto_now_add=True)


class RequestFiles(models.Model):
    linkDocs = models.TextField(blank=True)
    dateOn = models.DateTimeField(default=now)
    isDeleted = models.BooleanField(default=False)
    student_files = models.ForeignKey(Student, blank=True, null=True, on_delete=models.CASCADE)
    # name = models.CharField(max_length=256)
    # todo


class Request(models.Model):
    student = models.ForeignKey(Student, on_delete=models.SET_NULL, null=True)
    compaing = models.ForeignKey(Compaing, on_delete=models.SET_NULL, null=True)
    criterion = models.ForeignKey(DictCriterion, on_delete=models.SET_DEFAULT, default='нет критерия')
    sub_criterion = models.ForeignKey(DictSubCriterion, blank=True, null=True, default=1,
                                      on_delete=models.SET_DEFAULT)
    last_status = models.CharField("Текущий статус",
                                   max_length=156)  # Статусы: Черновик, На доработке, На проверке, Принято, Отказать по решению Стипендиальной Комиссии,Получение выплаты, Удалено(для админов)
    CreatedOn = models.DateTimeField("Дата создания", default=now)
    LastUpdate = models.DateTimeField("Последнее изменеиние", default=now)
    isDeleted = models.BooleanField("Удалено", default=False)
    comments = models.ManyToManyField(Comments, blank=True)
    protocolNumber = models.CharField('Номер протокола', max_length=256, null=True, blank=True, default=None)
    protocolDate = models.DateTimeField("Дата протокола", default=now)
    isValid = models.BooleanField(default=False)  # когда принято => True
    files = models.ManyToManyField(RequestFiles, blank=True)

    class Meta:
        ordering = ['-id']


class Notification(models.Model):
    """ Объявления """
    text = models.CharField(max_length=500)
    created_at = models.DateTimeField(auto_now_add=True)


class HistoryChangeRequest(models.Model):
    '''История изменения данных заявки'''
    student = models.ForeignKey(Student, on_delete=models.CASCADE)
    date = models.DateTimeField("Дата изменения", auto_now_add=True)
    json = models.JSONField()


class ExcelWordFiles(models.Model):
    path_to_file = models.CharField(max_length=200, blank=True, null=True)
