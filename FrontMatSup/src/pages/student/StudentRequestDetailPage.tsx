  import React, { FC, useContext, useEffect, useRef, useState } from 'react'
  import MediaQuery from "react-responsive"
  import { useParams, useNavigate } from 'react-router-dom'
  import { StudentHeader } from '../../components/Header'
  import { RequestContext } from '../../store/RequestContext'
  import { CompanyContext } from '../../store/CompanyContext'
  import { FormField } from '../../components/formFieldInput'

  import { AuthContext } from '../../store/AuthContext'
  import { Link } from 'react-router-dom'
  import M from 'materialize-css'
  import "../../index.css"
  import { useFormater } from '../../hooks/useFormater'
  import $api from '../../http'
  import { bottom, end } from '@popperjs/core'


  export const StudentRequestDetailPage: FC = () => {
    const { id1 } = useParams()
    const pointRef = useRef(null)
    const messageRef = useRef(null)
    const { requests, fetchRequests, addComment, setStatus } = useContext(RequestContext)
    const { fio, avatarUrl, role, id } = useContext(AuthContext)
    const request = requests.find(r => r.id === Number(id1))
    const [message, setMessage] = useState('')
    const [files, setFiles] = useState<{ id: number; linkDocs: string, dateOn: Date }[]>([])
    const [isFilesLoaded, setIsFilesLoaded] = useState(false)


    const [fileIamge, fileImageSet] = useState<File|null>()

    const [fileNameInput, fileNameInputSet] = useState('')
    const _ = useFormater()
    const navigate = useNavigate()

    const loadFiles = async () => {
      const resp = await $api.get('/api/requests-files/get/?pk=' + request?.id)
      setFiles(resp.data)
      
    }
    

    useEffect(() => {
      if (requests.length) fetchRequests()
      //if (request?.student.id !== id) navigate('/companies/')
    }, [])

    useEffect(() => {
      // @ts-ignore
      if (pointRef.current) pointRef.current.focus()
      M.CharacterCounter.init(messageRef.current!)

      if (request && !isFilesLoaded) {
        loadFiles().then(() => setIsFilesLoaded(true))
      }
    }, [request, isFilesLoaded])

    useEffect(() => {
      document.querySelectorAll('.tooltipped').forEach(el => {
        const url = el.getAttribute('data-tooltip-img')
        M.Tooltip.init(el, {
          html: `<img src="${url}" class="tooltip-img" />`,
        })
      })
    })

    const sendHandler = () => {
      try {
        if (message.trim().length === 0)
          return M.toast({
            html: `<span>Что-то пошло не так: <b>Комментрий не должен быть пустым!</b></span>`,
            classes: 'red darken-4 position',
          })

        addComment(request?.id!, fio, avatarUrl, message, role, id)
        setMessage('')
        M.toast({
          html: 'Вы успешно оставили комментарий!',
          classes: 'light-blue darken-1 position',
        })
      } catch (e) {
        // M.toast({
        //   html: `<span>Что-то пошло не так: <b>${e}</b></span>`,
        //   classes: 'red darken-4',
        // })
      }
    }

    if (!request && isFilesLoaded) {
      return (
        <>
          <StudentHeader />
          <div className='my-center'>
            <div className='preloader-wrapper big active'>
              <div className='spinner-layer spinner-blue-only'>
                <div className='circle-clipper left'>
                  <div className='circle'></div>
                </div>
                <div className='gap-patch'>
                  <div className='circle'></div>
                </div>
                <div className='circle-clipper right'>
                  <div className='circle'></div>
                </div>
              </div>
            </div>
          </div>
        </>
      )
    }

    function formatDate(date: string | number | Date) {
      let d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

      if (month.length < 2)
        month = '0' + month;
      if (day.length < 2)
        day = '0' + day;
      return [year, month, day].join('-');;
    }
    
    const endDate = new Date(Date.parse(String(request?.company.endDate)))
    const statusApplication =  (request?.status == "Черновик" || request?.status == "Отправлено на доработку") && endDate != undefined  && endDate > new Date();
    
    return (
      <>
      
        <StudentHeader />      
        <div style={{ marginRight: 70, marginLeft: 70 }}>
          <MediaQuery maxWidth={900}>
            <div className='row'>
              <div className='col s12'>
              <h3 className='mt-4'>Информация о заявлении</h3>
              <FormField
                  label={'Кампания'}

                  type={'text'} 
                  value={request?.company.name}     
              />
              <FormField
                  label={'Критерий'}

                  type={'text'} 
                  value={request?.nomination.name}     
              />
                
                <FormField
                  label={'Подкритерий'}

                  type={'text'} 
                  value={request?.subCriterion?.name}     
              />
                <FormField
                  label={'Статус'}

                  type={'text'} 
                  value={request?.status}     
              />
                <FormField
                  label={'Дата создания'}

                  type={'text'} 
                  value={_(request?.createdDate)}     
              />
              </div>
            </div>
          </MediaQuery>
        <MediaQuery minWidth={1200}>
          <h3 className='mt-4'>Информация о заявлении</h3>
          <table className='responsive-table centered com-4'>
            <thead>
              <tr>
                <th>Кампания</th>
                <th>Критерий</th>
                <th>Подкритерий</th>
                <th>Статус</th>
                <th>Дата создания</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>{request?.company.name}</td>
                <td>{request?.nomination.name}</td>
                <td>{request?.subCriterion?.name}</td>
                <td>{request?.status}</td>
                <td>{_(request?.createdDate)}</td>
              </tr>
            </tbody>
          </table>
          </MediaQuery>

          <MediaQuery maxWidth={900}>
            {(request?.status === 'Черновик' ||
              request?.status === 'Отправлено на доработку') && endDate > new Date() ? (
              <div
                style={{
                float: 'left',
                }}
                >
                  <Link to={`/edit-application/${request?.id}`}> <a className='waves-effect waves-light btn light-blue darken-1  com-4 margin-bm'>Информация о студенте</a></Link>
                        
                      </div>
                    ) : null}
          </MediaQuery>

          <MediaQuery minWidth={1200}>
          <h3 className='mt-4'>Информация о студенте</h3>


          {(request?.status === 'Черновик' ||
            request?.status === 'Отправлено на доработку') && endDate > new Date() ? (
            <div
              style={{
                float: 'left',
              }}
            >
              
              <Link className='waves-effect light-blue btn-small ' to={`/edit-application/${request?.id}`}>Редактировать</Link>
              
            </div>
          ) : null}

            
          <table className='responsive-table centered table-width com-4'>
            <thead>
              <tr>
                <th>ФИО</th>
                <th>Телефон</th>
                <th>Институт</th>
                <th>Направление</th>
                <th>Форма обучения</th>
                <th>Источник финансирования</th>
                <th>Уровень образования</th>
                <th>Курс</th>
                <th>ИНН</th>
                <th>СНИЛС</th>
                <th>Адрес прописки</th>
                
              </tr>
            </thead>
            <tbody >
              <tr>
                <td>{request?.student.fio}</td>
                <td>{request?.student.phone}</td>
                <td >{request?.student.institute}</td>
                <td>{request?.student.direction}</td>
                <td>{request?.student.educationForm}</td>
                <td>{request?.student.financingSource}</td>
                <td>{request?.student.level}</td>
                <td>{request?.student.course}</td>
                <td>{request?.student.INN}</td>
                <td>{request?.student.SNILS}</td>
                <td>{request?.student.address}</td>
              </tr>
            </tbody>
          </table>
          
          <h3 className='mt-4'>Персональные данные студента</h3>

          <table className='responsive-table centered table-width com-4'>
            <thead>
              <tr>
                <th className='w-3' >Адрес проживания</th>
                  <th className='w-1' >Гражданство</th>
                  <th className='w-1'>Серия и номер паспорта</th>
                  <th className='w-4' >Кем и когда выдан</th>
                  <th className='w-1'>Код департамента</th>
                </tr>
            </thead>

            <tbody>
              <tr>
                <td className='text-center' >{request?.student.fatcaddress}</td>
                <td className='text-center'>{request?.student.citizenship}</td>
                <td className='text-center'>
                    {request?.student.passport_seria}{' '}
                    {request?.student.passport_number}
                </td>
                <td className='text-center'>
                    {request?.student.passport_IssueBy}{' '}
                    {request?.student.passport_IssueDate}
                </td>
                <td className='text-center'>{request?.student.passport_DepartmentCode}</td>
                </tr>
            </tbody>
          </table>
          </MediaQuery>
          
            <MediaQuery maxWidth={900}>
              <Link to={`/add-document/${request?.id}`}>
                <a className='waves-effect waves-light btn light-blue darken-1 com-4'>Приложения к заявлению</a>
              </Link>
            </MediaQuery>

          <MediaQuery minWidth={1200}>
          <h3 className='mt-4'>Приложения к заявлению</h3>
          <table className='com-4'>
            <thead>
              <tr>
                <th >Документы к заявлению</th>
                <th><h6 className='required-files' >ОБЯЗАТЕЛЬНО ПРИКРЕПИТЬ! Паспорт: 2-3 стр, прописку, семейное положение по паспорту, ИНН, СНИЛС</h6></th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>{request?.nomination.docs}</td>
                <td>
                  <div className='row'>
                    {files.map(f => (
                      <>
                        <div className='col s2' style={{ position: 'relative' }}>
                                                    <td>
                              <small className='small' >{formatDate(f.dateOn).toLocaleString()}</small>
                              
                            </td>
                            
                          <a

                            key={f.id}
                            className='waves-effect waves-light btn light-blue darken-1 tooltipped '
                            href={f.linkDocs}
                            data-position='top'
                            data-tooltip-img={f.linkDocs}
                          >
                            <i className='material-icons'>insert_drive_file</i>

                          </a>
                            
                          <a
                            className='btn-floating btn-large waves-effect waves-light red darken-1 btn-small'
                            style={{
                              position: 'absolute',
                              bottom: -16.2,
                              left: 0,
                            }}
                            

                            onClick={async event => {
                              event.preventDefault()
                              if (request?.status == 'Черновик') {
                                $api.delete('/api/set-image/', {
                                  data: {
                                    id: request!.id,
                                    fileId: f.id,
                                  },
                                })
                                
                                setFiles(prev =>
                                  prev.filter(prevFile => prevFile.id !== f.id)
                                )
                              }
                              if (request?.status == "Отправлено на доработку") {
                                $api.delete('/api/set-image/', {
                                  data: {
                                    id: request!.id,
                                    fileId: f.id,
                                  },
                                })

                                setFiles(prev =>
                                  prev.filter(prevFile => prevFile.id !== f.id)
                                )
                              }
                            }}
                          >
                            <i className='material-icons'>close</i>
                          </a>
                          <td><small className='small' >{f.linkDocs.substring(f.linkDocs.lastIndexOf('/') + 1)}</small></td>
                        </div>
                        
                      </>
                    ))}
                    
                  </div>
                  
                </td>
                
              </tr>
            </tbody>
          </table>
          {request?.status === 'Черновик' ||
            request?.status === 'Отправлено на доработку' ? (
            <div
              className='button-input-container'
              style={{
                float: 'right',
              }}
            >
              <div className='file-field input-field'>
                <div className='btn light-blue darken-2' style={{width:"80%"}} >
                  <label className='add-label com-4'   htmlFor="files">Добавить файл</label>
                  <input
                    className='hidden-input'
                    id="files"
                    type='file'
                    onChange={async (
                      event: React.ChangeEvent<HTMLInputElement>
                    ) => {
                      try {
                        
                        const file = event.target.files![0]
                        fileImageSet(file)
                        fileNameInputSet(file.name)
                        
                      } catch (e) {
                        // M.toast({
                        //   html: `<span>Что-то пошло не так: <b>${e}</b></span>`,
                        //   classes: 'red darken-4',
                        // })
                      }
                    }}
                  />
                
                  <input
                    id="fileName"
                    
                    value={fileNameInput}
                    onChange={(e) => fileNameInputSet(e.target.value)}
                  />
                  <div>
                      <button
                      className="btn light-blue darken-2 com-4"
                      onClick={ async () =>  {
                        console.log(typeof(fileNameInput))
                        if (fileNameInput.indexOf('.pdf') !== -1 || fileNameInput.indexOf('.jpeg') !== -1 || fileNameInput.indexOf('.png') !== -1 || fileNameInput.indexOf('.jpg') !== -1 || fileNameInput.indexOf('.heic') !== -1) {
                          const fd = new FormData()

                        fd.append('image', fileIamge as File, fileNameInput)
                        fd.append('request', request.id.toString())
                        const resp = await $api.post('/api/set-image/', fd)

                        if (resp.status === 201){
                          fileImageSet(null)
                          fileNameInputSet('')
                        }else{
                          alert('Что-то пошло не так!')
                        }
                        
                        document.querySelectorAll('.tooltipped').forEach(el => {
                          const url = el.getAttribute('data-tooltip-img')
                          M.Tooltip.init(el, {
                            html: `<img src="${url}" class="tooltip-img" />`,
                        })
                      })

                      setFiles
                      (prev => [...prev, resp.data])
                          
                        } else {
                          alert('Не указано расширение файла (pdf, png, jpg, jpeg, heic)')
                        }
                        
                      }
                          
                        }
                        type="submit"
                        name="action"
                        >Сохранить
                      </button>
                      <h6 className='required-files com-4' id='file-warning' >Обязательно переименуйте файл!</h6>
                  </div>
                  
                  
                </div>
                <div className='file-path-wrapper'>
                  <input className='file-path validate' type='text' />
                </div>
              </div>
            </div>
          ) : null}

        </MediaQuery>
          
          <br />
          <h3 className='mt-4'>Комментарии</h3>
          <div style={{
            margin: "5% 0% "
          }} >
            {request?.comments.map((c, idx) => {
              return (
                <div key={idx} className='comment'>
                  <div className='avatar'>
                    {/* <img src={c.imageUrl} alt='avatar' /> */}
                    <span>{c.name}</span>
                    </div>
                  <p className='com-3'>{c.text}</p>
                  
                  <small>{c.sendedDate.toLocaleString()}</small>
                  
                </div>
              )
            })}
          </div>
          <div className='input-field'>
            <textarea
              id='message'
              className='materialize-textarea mt-4'
              data-length='1000'
              value={message}
              onChange={(event: React.ChangeEvent<HTMLTextAreaElement>) => {
                if (message.length <= 1000) setMessage(event.target.value)
              }}
              ref={messageRef}
            ></textarea>
            <label className='message com-4'>Сообщение</label>
          </div>
          
          <button
            className='btn light-blue darken-2 waves-effect waves-light center-btn com-4' 
            style={{ float: 'right' }}
            onClick={sendHandler}
          >Отправить комментарий
            <i className='material-icons right'>send</i>
            
          </button>
          
          {(statusApplication) ? 
              <div className='displayed'>
              <button     
                          style={{float: 'right'}}
                          className='btn light-blue darken-2 waves-effect waves-light left com-4'
                          onClick={() => {
                            try {
                              setStatus(request?.id!, 'Отправлено на рассмотрение')
                              addComment(
                                request?.id!,
                                fio,
                                avatarUrl,
                                'Статус изменён на "Отправлено на рассмотрение"',
                                role,
                                id
                              )
        
                              M.toast({
                                html: '<span>Вы успешно выставили статус <strong>Отправлено на рассмотрение</strong> !</span>',
                                classes: 'light-blue darken-1 position',
                              })
                            } catch (e) {
                              M.toast({
                                html: `<span>Что-то пошло не так: <b>${e}</b></span>`,
                                classes: 'red darken-4 position',
                              })
                            }
                          }}
                        >
                          Отправить на рассмотрение
                        </button>
        
              </div>
                
                : <div></div>
            }
        
        <div style={{ height: 100 }}></div>

        
          
        </div>
        
      </>
    )
  }
